(function () {
    let me = {};
    const form = document.querySelector('.form__wrap');
    const buttOpen = document.querySelector('.form__arrow');
    const buttClose = document.querySelector('.form__close');
    const buttSubmit = document.querySelector('.form__submit');
    me.openForm = function () {
        form.classList.remove('is-hidden');
        buttClose.addEventListener('click', onClose);

        function onClose() {
            form.classList.add('is-hidden');
            me.closeForm();
        }
    };
    me.closeForm = function () {
        removeEventListener('click', onclose);

    };
    buttOpen.addEventListener('click', me.openForm);

    //-------------form--------------------------------
    me.isValid = function (e) {
        let email = document.querySelector('[data-email]').value;
        let phone = document.querySelector('[data-phone]').value;
        if (!noEmpty()) {
           alert('Заполните все поля');
        } else if (!RUS.validation.isEmail(email)) {
           alert('Неверный Email');
        }else if (!RUS.validation.isNumber(phone)) {
           alert('Неверный номер');
        }else {
           alert('Отправляем');}
    };
        function noEmpty() {
        let imp = document.querySelectorAll('[data-valid="required"]');
        for(let i=0;i<imp.length;i++) {
            let value = imp[i].value;
            let res = RUS.validation.isnotEmpty(value);
            if (!res) {
                return false;
            }
        }
            return true;
    };
    form.addEventListener('submit', function (e) {
        e.preventDefault();
        me.isValid(e);
    });

    //----------------------block ads--------------
    var blockAds = document.body.children[0];
    blockAds.css('display:none')


    RUS.form = me;


}());
